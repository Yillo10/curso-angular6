import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { ActivatedRoute } from '@angular/router';
import { DestinosApiClient } from './../../models/destinos-api-client.model';

/*
class DestinosApiClientViejo {
  getById(id: String): DestinoViaje {
    console.log('Llamando por la clase vieja');
    return null;
  }
}
interface AppConfig {
  apiEndpoint: string;
}
const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'mi_api.com'
};
const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

class DestinosApiClientDecorated extends DestinosApiClient {
  constructor( @Inject(APP_CONFIG) private config: AppConfig, store: Store<AppState> ){
    super(store);
  } 
  getById(id: String): DestinoViaje{
    console.log('Llamando a la clase decorada');
    console.log('config: '+ this.config.apiEndpoint);
    return super.getById(id);
  }
}/*/
@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [ DestinosApiClient ] 
})

export class DestinoDetalleComponent implements OnInit {
  destino: DestinoViaje;
  style = {
    sources: { //este source no es de mapbox
      world:{
        type:'geojson',
        data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
      }
    },
    versio: 8,
    layers: [{
      'id': 'countries',
      'type': 'fill',
      'source': 'world',
      'layout': {},
      'paint:': {
       'fill-color': '#6f788a'
      } 
    }]
  };

  constructor(private route: ActivatedRoute, private destinosApiClient: DestinosApiClient) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.destino = this.destinosApiClient.getById(id);
    //this.destino=null;
  } 

}
