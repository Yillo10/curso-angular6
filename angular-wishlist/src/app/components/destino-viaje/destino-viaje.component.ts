import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { Store } from '@ngrx/store';
import { AppState } from './../../app.module';
import { VoteUpAction, VoteDownAction } from './../../models/destinos-viajes-state.model';
import { trigger, state, transition, animate, style} from '@angular/animations';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css'],
  animations: [
    trigger('esfavorito', [
      state('estadoFavorito', style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'WhoteSmoke'
      })),
      transition('estadoNoFavorito => estadoFavorito', [
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('1s')
      ]),
    ])
  ]
})

export class DestinoViajeComponent implements OnInit {
  @Input() destino: DestinoViaje;
  @Input() position: number;
  // @Input('idx') position: number;  -> RENOMBRANDO UNA VARIABLE PARAMETROS CUSTOM <- //
  @HostBinding('attr.class') cssClass='col-md-4 mb-10';
  @Output() clicked: EventEmitter<DestinoViaje>

  constructor(private store: Store<AppState>) {
    this.clicked = new EventEmitter();
  }

  ngOnInit(): void {
  }

  ir(){
    this.clicked.emit(this.destino);
    return false;
  }

  voteUp(){
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }
  voteDown(){
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }
 

}
