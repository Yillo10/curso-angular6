import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { Store, State } from '@ngrx/store';
import { AppState } from './../../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './../../models/destinos-viajes-state.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [
    DestinosApiClient
  ]
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAded: EventEmitter<DestinoViaje>;
  updates: string[];
  all;

  constructor(
    private destinosApiClient:DestinosApiClient,
    private store: Store<AppState>
    ) { 
    this.onItemAded= new EventEmitter();
    this.updates= [];
  }

  ngOnInit(): void {
    this.store.select(state => state.destinos.favorito)
      .subscribe(d => {
        if(d != null){
          this.updates.push('se a elegido a: '+ d.nombre);
        }
     })
     //store.select(state => state.destinos.items).subscribe(items => this.all = items);
  } 

  agregado(d: DestinoViaje){
    this.destinosApiClient.add(d);
    this.onItemAded.emit(d);
  }

  elegido(e: DestinoViaje){
    this.destinosApiClient.elegir(e);
  }
  getAll(){ }
 
}
