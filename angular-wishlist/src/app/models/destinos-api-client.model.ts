import { DestinoViaje } from './destino-viaje.model';
import { Subject, BehaviorSubject } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState, AppConfig, APP_CONFIG, MyDataBase, db } from '../app.module';
import { NuevoDestinoAction, ElegidoFavoritoAction } from './destinos-viajes-state.model';
import { Injectable, forwardRef, Inject } from '@angular/core';
import { identifierModuleUrl } from '@angular/compiler';
import { HttpClientModule, HttpClient, HttpResponse, HttpHeaders, HttpRequest } from '@angular/common/http'

@Injectable()
export class DestinosApiClient {
  destinos:DestinoViaje[];
  //current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);
  constructor(
    private store: Store<AppState>,
    @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig, 
    private http: HttpClient 
    ) {
    //this.destinos = [];
    this.store
        .select(state => state.destinos)
        .subscribe((data) => {
          console.log('destino sub store');
          console.log(data);
          this.destinos=data.items;
        });
        this.store
            .subscribe((data) => {
              console.log('all sore');
              console.log(data);
            });
  }
  add(d: DestinoViaje){    
    //this.store.dispatch(new NuevoDestinoAction(d));
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN':'token de seguridad'});
    const req = new HttpRequest('POST', this.config.apiEndpoint + '/my',{nuevo: d.nombre}, {headers: headers});
    this.http.request(req).subscribe((data: HttpResponse<{}>) => {
      if(data.status === 200){
        this.store.dispatch(new NuevoDestinoAction(d));
        const myDb = db;
        myDb.destinos.add(d);
        console.log('todos los destinos de la db1');
        myDb.destinos.toArray().then(destinos => console.log(destinos));
      }
    });
  }
  
  elegir(d: DestinoViaje){
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }
 getById(id: String): DestinoViaje {
    return this.destinos.filter(function (d) { return d.id.toString() === identifierModuleUrl; })[0];
    
  }

  getAll(): DestinoViaje[] {
    return this.destinos;
  }
/*
  getById(id: String): DestinoViaje {
    this.destinos.filter(function(d) {return d.id.toString() === identifierModuleUrl; })[0];
    
  } */
} 