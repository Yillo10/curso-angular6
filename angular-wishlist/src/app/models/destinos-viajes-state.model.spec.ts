import {
    reducersDestinosViajes,
    DestinosViajesState,
    intializeDestinosViajesState,
    InitMyDataAction,
    NuevoDestinoAction
} from './destinos-viajes-state.model';
import {DestinoViaje} from './destino-viaje.model';

describe('reducersDestinosViajes', () => {
    it('should reduce init data', () => {
        //SETUP (CONFIGURACION DE LA INICIALIZACION)
        const prevState: DestinosViajesState = intializeDestinosViajesState();
        const action: InitMyDataAction= new InitMyDataAction(['destino 1', 'destino 2']);
        //ACCIONES(ACTUAR SOBRE EL DOMINIO PRODUCTIVO)
        const newState: DestinosViajesState = reducersDestinosViajes(prevState, action);
        //ASSERT (VALORES RETORMADOS PARA VALIDACIONES  *VERIFICACIONES*)
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].nombre).toEqual('destino 1');
    });

    it('should reduce new item added', () => {
        const prevState: DestinosViajesState = intializeDestinosViajesState();
        const action: NuevoDestinoAction= new NuevoDestinoAction(new DestinoViaje('barcelona','url'));
        const newState: DestinosViajesState = reducersDestinosViajes(prevState, action);
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].nombre).toEqual('barcelona');
    });

});

